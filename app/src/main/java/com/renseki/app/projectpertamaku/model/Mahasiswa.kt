package com.renseki.app.projectpertamaku.model

import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable

data class Mahasiswa(
        val nrp: String,
        val name: String,
        val gender: Gender
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            when(parcel.readByte()) {
                0.toByte() -> Gender.MALE
                else -> Gender.FEMALE
            }
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(nrp)
        parcel.writeString(name)
        parcel.writeByte(
            when(gender) {
                Gender.MALE -> 0
                Gender.FEMALE -> 1
            }
        )
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Mahasiswa> {
        override fun createFromParcel(parcel: Parcel): Mahasiswa {
            return Mahasiswa(parcel)
        }

        override fun newArray(size: Int): Array<Mahasiswa?> {
            return arrayOfNulls(size)
        }
    }
}